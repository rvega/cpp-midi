#include <jack/midiport.h>

/**
 * Some typedefs and defines for readability and portability
 */
typedef uint64_t int64;
typedef uint8_t int8;
