#include "ofMain.h"
#include "GUI.h"
#include "Messaging.h"

GUI::GUI(){}

void GUI::setup(){
  ofSetBackgroundColor(30,30,30);
  ofSetFrameRate(60);
  ofSetWindowPosition(0, 0);

  ofxDatGui* gui = new ofxDatGui(ofxDatGuiAnchor::TOP_LEFT);

  ofColor yellow = ofColor::fromHex(0xFFD00B);
  ofxDatGuiFolder* f1 = gui->addFolder("DSP PARAMETERS", yellow);
  f1->addSlider("  Slider", 0, 1);
  f1->addButton("  Button");
  f1->addBreak();
  f1->expand();

  ofColor green = ofColor::fromHex(0x1ED36F);
  ofxDatGuiFolder* f2 = gui->addFolder("TEXT OUTPUT", green);
  f2->addButton("  Copy");
  f2->addButton("  Clear");
  f2->expand();

  gui->setTheme(new AppGuiTheme());

  f1->onButtonEvent(this, &GUI::clickedButton);
  f1->onSliderEvent(this, &GUI::changedSlider);
  f2->onButtonEvent(this, &GUI::clickedButton);
  f2->onSliderEvent(this, &GUI::changedSlider);

  textElement.load("ofxdatgui_assets/font-verdana.ttf", 10);
  text = "";
}

void GUI::draw(){
  textElement.drawString(text, 340, 20);
}

void GUI::update(){
  MessageType t;
  char v[STRING_MESSAGE_SIZE];
  string str;
  while(MessageMan->getStringInGUI(&t,v)){
    str = v;
    if(str.length() + text.length() >= 10000){
      text = "";
    }
    text = str + "\n" + text;
  }
}


void GUI::clickedButton(ofxDatGuiButtonEvent e){
  if(e.target->is("  Clear")){
    text = "";
  }
  else if(e.target->is("  Copy")){
    ofGetWindowPtr()->setClipboardString(text); 
  }
  else if(e.target->is("  Button")){
    MessageMan->sendFloatToDSP(BANG, 1.0);
  }
}

void GUI::changedSlider(ofxDatGuiSliderEvent e){
  if(e.target->is("  Slider")){
    MessageMan->sendFloatToDSP(SLIDER, e.target->getValue());
  }
}
