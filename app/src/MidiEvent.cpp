#include "MidiEvent.h"
#include <stdlib.h>

MidiEvent::MidiEvent(){}

// Operator "=" for easy copying
MidiEvent& MidiEvent::operator=(const MidiEvent& original){
  if(this==&original) return *this;

  this->time = original.time;
  this->size = original.size;
  for(int i=0; i<original.size; ++i){
    this->bytes[i] = original.bytes[i];
  }

  return *this;
}

void MidiEvent::setRaw(int8* bytes, int size, int time){
  this->time = time;
  this->size = size;
  for(int i=0; i<size; ++i){
    this->bytes[i] = bytes[i];
  }
}

void MidiEvent::getRaw(int8** bytes, int* size, int* time){
  *time = this->time;
  *size = this->size;
  *bytes = this->bytes;
}

bool MidiEvent::isNoteOn(){
  return (size==3 && ((bytes[0])&0xF0)==0x90);
}

void MidiEvent::parseNoteOn(int* note, int* velocity, int* channel){
  *note = bytes[1];
  *velocity = bytes[2];
  if(channel) *channel = bytes[0] & 0x0F;
}

void MidiEvent::setNoteOn(int note, int velocity, int channel){
  size = 3;
  bytes[0] = 0x90 | (channel & 0x0F);
  bytes[1] = note;
  bytes[2] = velocity;
}

bool MidiEvent::isNoteOff(){
  bool itIsNoteOff = (size==3 && (bytes[0]&0xF0)==0x80);
  bool isNoteOnVelocityZero = (size==3 && (bytes[0]&0xF0)==0x90 && bytes[2]==0);
  return itIsNoteOff || isNoteOnVelocityZero;
}

void MidiEvent::parseNoteOff(int* note, int* velocity, int* channel){
  *note = bytes[1];
  *velocity = bytes[2];
  if(channel) *channel = bytes[0] & 0x0F;
}

void MidiEvent::setNoteOff(int note, int velocity, int channel){
  size = 3;
  bytes[0] = 0x90 | (channel & 0x0F);
  bytes[1] = note;
  bytes[2] = velocity;
}


