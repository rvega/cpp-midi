#include "Messaging.h"
#include <string>
#include <iostream>


/**
 * StringMessage
 */
StringMessage::StringMessage(MessageType t, const char* v){
  type = t;
  assert(strlen(v)<STRING_MESSAGE_SIZE && "Message too long");
  strncpy(value, v, STRING_MESSAGE_SIZE);
}
    
StringMessage::StringMessage(){
  type = PRINT;
  strncpy(value, "", STRING_MESSAGE_SIZE);
};

/**
 * FloatMessage
 */
FloatMessage::FloatMessage(){
  // type = VOLUME;
  value = 0;
};

FloatMessage::FloatMessage(MessageType t, float v){
  type = t;
  value = v;
}

/**
 * Messaging
 */

// Initialize static members
Messaging Messaging::sharedInstance;
char Messaging::messageToGui[STRING_MESSAGE_SIZE];

Messaging::Messaging(){
  PaUtil_InitializeRingBuffer(&rbFloatToDSP, sizeof(FloatMessage), MESSAGE_BUFFER_SIZE, &rbFloatToDSPData);
  PaUtil_InitializeRingBuffer(&rbStringToGUI, sizeof(StringMessage), MESSAGE_BUFFER_SIZE, &rbStringToGUIData);
  PaUtil_InitializeRingBuffer(&rbAudioToGUI, sizeof(float), AUDIO_BUFFER_SIZE, &rbAudioToGUIData);
}

Messaging* Messaging::getSharedInstance(){
  return &sharedInstance;
}

void Messaging::sendFloatToDSP(MessageType type, float value){
  FloatMessage msg(type, value);
  PaUtil_WriteRingBuffer(&rbFloatToDSP, &msg, 1);
}

int Messaging::getFloatInDSP(MessageType* t, float* f){
  FloatMessage msg;
  int count = PaUtil_ReadRingBuffer(&rbFloatToDSP, &msg, 1);
  *t = msg.type;
  *f = msg.value;
  return count;
}

void Messaging::sendStringToGUI(MessageType type, const char* value){
  StringMessage msg(type, value);
  PaUtil_WriteRingBuffer(&rbStringToGUI, &msg, 1);
}

int Messaging::getStringInGUI(MessageType* t, char* v){
  StringMessage msg;
  int count = PaUtil_ReadRingBuffer(&rbStringToGUI, &msg, 1);
  *t = msg.type;
  strncpy(v, msg.value, STRING_MESSAGE_SIZE);
  return count;
}

void Messaging::sendAudioToGUI(const float* audio, int samples){
  PaUtil_WriteRingBuffer(&rbAudioToGUI, audio, samples);
}

int Messaging::getAudioInGUI(float* audio, int count){
  int read = PaUtil_ReadRingBuffer(&rbAudioToGUI, audio, count);
  return read;
}
