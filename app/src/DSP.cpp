#include "DSP.h"
#include "AudioMidiIO.h"

DSP::DSP(){}

/////////////////////////////////////////////////////////////////////
// The following methods will be called automatically by AudioMidiIO
//

void DSP::audioParametersChanged(int bufSize, int sampRate){
  // Use this method to to do any memory allocations based on buffer 
  // size or sample rate changes, to tell other objects that the
  // sample rate changed, etc.
}

void DSP::setup(AudioContext* c){
  TimeMan->schedule(clock1ms, 1.0, (void*)this);
}

int DSP::process(int nframes){
  for(int frame=0; frame<nframes; ++frame){
    // Do your audio processing here...
    audioContext->tick(frame);
  }
  return 0;
} 

void DSP::incomingMidi(MidiEvent* evt){
  // Pass through
  outgoingMidiEvent = *evt;
  audioContext->outputMidi(&outgoingMidiEvent);
  
  // Current time in seconds (for printing)
  float current = TimeMan->getCurrentTime();
  float sampleRate = audioContext->sampleRate;
  float seconds = current/sampleRate;

  if(evt->isNoteOn()){
    int channel, note, velocity;
    evt->parseNoteOn(&note, &velocity, &channel);
    DSP_PRINT("IN: NoteOn [%.4f] %i %i %i", seconds, note, velocity, channel);
  }
  else if(evt->isNoteOff()){
    int channel, note, velocity;
    evt->parseNoteOff(&note, &velocity, &channel);
    DSP_PRINT("IN: NoteOff [%f] %i %i %i", seconds, note, velocity, channel);
  }
}

/////////////////////////////////////////////////////////////////////
// The following methods are custom/application specific
//

void DSP::clock1ms(Scheduler* s, int64 time, void* arg){
  // The "this" pointer is not available because this is a static 
  // method (It has to be static so we can call it from TimeMan).
  // We do this "self" trick instead. Notice that when we tell TimeMan
  // to schedule the clock1ms function, we pass the "this" pointer.
  DSP* self = (DSP*)arg;

  self->handleGuiEvents();
  
  TimeMan->schedule(clock1ms, 1, arg);
}

void DSP::handleGuiEvents(){ 
  MessageType t;
  float v;
  while(MessageMan->getFloatInDSP(&t, &v)){
    if(t==BANG){
      DSP_PRINT("%s", "Got bang!");
    }
    else if(t==SLIDER){
      DSP_PRINT("Got slider value %f", v);
    }
  }
}
