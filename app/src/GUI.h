#pragma once

#include "ofxDatGui.h"

/**
 * Gui theme. See vendors/ofxDatGui/src/themes
 */
class AppGuiTheme: public ofxDatGuiTheme{
  public:
    AppGuiTheme(){
      font.size = 8;
      layout.width = 300;
      init();
    }
};

class GUI{
  public:
    GUI();
    void setup();
    void update();
    void draw();

  private:
    ofTrueTypeFont textElement;
    string text;

    void clickedButton(ofxDatGuiButtonEvent e);
    void changedSlider(ofxDatGuiSliderEvent e);
};
