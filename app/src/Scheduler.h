#pragma once
#include <map>
#include "Definitions.h"

class SchedulerDelegate;
class Scheduler;

typedef void (*SchedulerCallback)(Scheduler*, int64, void*);

class SchedulerDelegate{
  public:
    SchedulerDelegate();
    SchedulerDelegate(SchedulerCallback cbk, void* a);
    SchedulerCallback callback;
    void* arg;
};

class Scheduler{
  public:
    Scheduler();
    static Scheduler* getSharedInstance();
    void tick();
    int64 getCurrentTime();
    void setCurrentTime(unsigned int time);
    void setSampleRate(int sampleRate);
    void schedule(SchedulerCallback cbk, float milliseconds, void* arg);

  private:
    // A "singleton" instance
    static Scheduler sharedInstance;

    int sampleRate;
    int64 currentTime;

    std::multimap<int64, SchedulerDelegate*> scheduleMap;
    std::multimap<int64, SchedulerDelegate*>::iterator scheduleIterator;
     
    // Prevent unwanted copies
    Scheduler(const Scheduler&);
    Scheduler& operator=(const Scheduler&);
};

#define TimeMan Scheduler::getSharedInstance()
