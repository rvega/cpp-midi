#pragma once

#include <jack/jack.h>
#include <jack/midiport.h>
#include "ofMain.h"
#include "DSP.h"


/**
 * A class to pass around data in audio callback
 */
class AudioContext{
  friend class AudioMidiIO;

  public:
    AudioContext();
    ~AudioContext();

    int sampleRate;
    int bufferSize;

    void tick(int frame);
    void outputMidi(MidiEvent* evt);

  protected:
    DSP* dsp;
    bool isFirstCallback;

    jack_client_t* jackClient;
    // jack_port_t** audioOutPorts;
    // float** audioOutBuffers;
    // jack_port_t** audioInPorts;
    // float** audioInBuffers;

    jack_port_t* midiInPort;
    void* midiInBuffer;
    MidiEvent incomingMidiEvent;
    int midiEventCount;
    int midiEventIndex;
    void newBlockSetupMidi(jack_nframes_t nframes);

    jack_port_t* midiOutPort;
    void* midiOutBuffer;

  private:
    // Prevent undesired copying
    AudioContext(const AudioContext&);
    AudioContext& operator=(const AudioContext&);
};


/**
 * This is the class that sets up connections with OS audio and midi
 * system
 */
class AudioMidiIO{
  public:
    AudioMidiIO();
    int start();
    void stop();

  private:
    DSP dsp;
    AudioContext context;

    static int jackProcess(jack_nframes_t nframes, void *arg);
    static void jackShutdown(void *arg);
    static int jackBufferSize(unsigned int newSampleRate, void *arg);
    static int jackSampleRate(unsigned int newSampleRate, void *arg);
    static int jackXrun(void *arg);
    
    // Prevent undesired copying
    AudioMidiIO(const AudioMidiIO&);
    AudioMidiIO& operator=(const AudioMidiIO&);
};
