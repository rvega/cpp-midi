#pragma once

#include <cstdint>
#include "Scheduler.h"
#include "Messaging.h"
#include "MidiEvent.h"

class AudioContext;

class DSP{
  friend class AudioMidiIO;

  public:
    AudioContext* audioContext;

    DSP();
    void setup(AudioContext* c);
    int process(int nframes);
    void audioParametersChanged(int bufSize, int sampRate);
    void incomingMidi(MidiEvent* evt);

  private:
    MidiEvent outgoingMidiEvent;

    static void clock1ms(Scheduler* s, int64 time, void* arg);
    void handleGuiEvents();
    
    // Prevent copies
    DSP(const DSP&);
    DSP& operator=(const DSP&);
};
