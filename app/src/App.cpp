#include <cassert>

#include "App.h"
#include "Messaging.h"

App::App(){}

void App::setup(){
  gui.setup();
  if(audioMidiIO.start()){
    std::cerr << "Cannot connect to jack server, bailing out." << std::endl;
    throw(std::exception());
  }
}

void App::update(){
  gui.update();
}

void App::draw(){
  gui.draw();
}


// //--------------------------------------------------------------
// void App::keyPressed(int key){
//
// }
//
// //--------------------------------------------------------------
// void App::keyReleased(int key){
//
// }
//
// //--------------------------------------------------------------
// void App::mouseMoved(int x, int y ){
//
// }
//
// //--------------------------------------------------------------
// void App::mouseDragged(int x, int y, int button){
//
// }
//
// //--------------------------------------------------------------
// void App::mousePressed(int x, int y, int button){
//
// }
//
// //--------------------------------------------------------------
// void App::mouseReleased(int x, int y, int button){
//
// }
//
// //--------------------------------------------------------------
// void App::mouseEntered(int x, int y){
//
// }
//
// //--------------------------------------------------------------
// void App::mouseExited(int x, int y){
//
// }
//
// //--------------------------------------------------------------
// void App::windowResized(int w, int h){
//
// }
//
// //--------------------------------------------------------------
// void App::gotMessage(ofMessage msg){
//
// }
//
// //--------------------------------------------------------------
// void App::dragEvent(ofDragInfo dragInfo){ 
//
// }
