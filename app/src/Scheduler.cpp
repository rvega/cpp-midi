#include "Scheduler.h"
#include <iostream>

SchedulerDelegate::SchedulerDelegate(){

}

SchedulerDelegate::SchedulerDelegate(SchedulerCallback cbk, void* a){
  callback = cbk;
  arg = a;
}

// Initialize static member
Scheduler Scheduler::sharedInstance;

Scheduler::Scheduler(){}

Scheduler* Scheduler::getSharedInstance(){
  return &sharedInstance;
}

void Scheduler::tick(){
  int64 t;
  SchedulerDelegate* d;
  for(scheduleIterator=scheduleMap.begin(); scheduleIterator!=scheduleMap.end(); ++scheduleIterator){
    t = (*scheduleIterator).first;
    if(currentTime >= t){
      d = (*scheduleIterator).second;
      d->callback(this, currentTime, d->arg);
      scheduleMap.erase(t);
      delete(d);
    }
  }

  currentTime++;
}

void Scheduler::setSampleRate(int sr){
  // TODO: if sample rate changes after scheduler is ticking, we need to do more stuff here. 
  sampleRate = sr;
}

int64 Scheduler::getCurrentTime(){
  return currentTime;
}

void Scheduler::setCurrentTime(unsigned int time){
  // Audio IO system sample counter is 32 bit, we want 64.
  int64 msb = currentTime & 0xFFFFFFFF00000000;
  currentTime = msb | time;
}

void Scheduler::schedule(SchedulerCallback cbk, float milliseconds, void* arg){
  // TODO pre-allocate a pool of delegates and measure performance ?
  
  int64 time = currentTime + (milliseconds * ((float)sampleRate/1000.0));
  SchedulerDelegate* delegate = new SchedulerDelegate(cbk, arg);
  scheduleMap.insert(std::pair<int64, SchedulerDelegate*>(time, delegate));
}
