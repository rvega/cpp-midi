#pragma once

#include "ofMain.h"
#include "DSP.h"
#include "GUI.h"
#include "AudioMidiIO.h"

class App: public ofBaseApp{
  public:
    App();
    void setup();
    void update();
    void draw();

  private:
    AudioMidiIO audioMidiIO;
    GUI gui;

    // Prevent undesired copying
    App(const App&);
    App& operator=(const App&);



    // void keyPressed(int key);
    // void keyReleased(int key);
    // void mouseMoved(int x, int y );
    // void mouseDragged(int x, int y, int button);
    // void mousePressed(int x, int y, int button);
    // void mouseReleased(int x, int y, int button);
    // void mouseEntered(int x, int y);
    // void mouseExited(int x, int y);
    // void windowResized(int w, int h);
    // void dragEvent(ofDragInfo dragInfo);
    // void gotMessage(ofMessage msg);

};
