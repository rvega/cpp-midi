#pragma once

#include <cassert>
#include <cstring>
#include <stdio.h>
#include "pa_ringbuffer.h"

#define MESSAGE_BUFFER_SIZE 128
#define AUDIO_BUFFER_SIZE 524288 // 96KHz * 2 sec * 2 channels, round to power of 2
#define STRING_MESSAGE_SIZE 64

/**
 * The message types. This is application specific
 */
enum MessageType{
  PRINT=0,
  BANG,
  SLIDER
};

/**
 * These datastructures are the messages actually being passed in the ring buffers
 */
class StringMessage{
  public:
    MessageType type;
    char value[STRING_MESSAGE_SIZE];
    StringMessage(MessageType t, const char* v);
    StringMessage();

  private:
    // Prevent undesired copying
    StringMessage(const StringMessage&);
    StringMessage& operator=(const StringMessage&);
};

class FloatMessage{
  public:
    MessageType type;
    float value;
    FloatMessage();
    FloatMessage(MessageType t, float v);

  private:
    // Prevent undesired copying
    FloatMessage(const FloatMessage&);
    FloatMessage& operator=(const FloatMessage&);
};

/**
 * This class provides the public interface for messaging
 * and holds state and data.
 */
class Messaging{
  private:
    // A "singleton" instance
    static Messaging sharedInstance;

    // Ring buffer data space, sizes must be power of 2
    FloatMessage rbFloatToDSPData[MESSAGE_BUFFER_SIZE];
    StringMessage rbStringToGUIData[MESSAGE_BUFFER_SIZE];
    float rbAudioToGUIData[AUDIO_BUFFER_SIZE];

    // The ring buffers
    PaUtilRingBuffer rbFloatToDSP;
    PaUtilRingBuffer rbStringToGUI;
    PaUtilRingBuffer rbAudioToGUI;

    // Prevent undesired copying
    Messaging(const Messaging&);
    Messaging& operator=(const Messaging&);

  public:
    // Factory for "singleton" instance
    static Messaging* getSharedInstance();

    // Constructor (usually no need to call it, use getSharedInstance)
    Messaging();
    
    // A pre-allocated string to send messages to GUI
    static char messageToGui[STRING_MESSAGE_SIZE];

    // Methods to be called from real-time threads
    int getFloatInDSP(MessageType* t, float* f);
    void sendStringToGUI(MessageType type, const char* value);
    void sendAudioToGUI(const float* audio, int samples);

    // Methods to be called from GUI
    void sendFloatToDSP(MessageType type, float value);
    int getStringInGUI(MessageType* t, char* v);
    int getAudioInGUI(float* audio, int count);
};

/**
 * Useful macros
 */

#define MessageMan Messaging::getSharedInstance()

#define DSP_PRINT(format, ...){ \
    char __msg[STRING_MESSAGE_SIZE]; \
    sprintf(__msg, format, __VA_ARGS__); \
    Messaging::getSharedInstance()->sendStringToGUI(PRINT, __msg); \
  }
  
#ifndef NDEBUG
  #define DSP_DEBUG(format, ...){ \
      char __msg[STRING_MESSAGE_SIZE]; \
      sprintf(__msg, format, __VA_ARGS__); \
      Messaging::getSharedInstance()->sendStringToGUI(PRINT, __msg); \
    }
#else
  #define DSP_DEBUG(msg) (void(0))
#endif
