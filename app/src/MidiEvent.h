#pragma once

#include "Definitions.h"

#define MAX_MIDI_EVENT_SIZE 3

class MidiEvent{
  public:
    MidiEvent();
    MidiEvent& operator=(const MidiEvent&);

    int time; // This is an offset in samples from the start of the audio block.

    void setRaw(int8* bytes, int size, int time);
    void getRaw(int8** bytes, int* size, int* time);

    bool isNoteOff();
    void parseNoteOff(int* note, int* velocity, int* channel=0);
    void setNoteOff(int note, int velocity=0, int channel=0);

    bool isNoteOn();
    void parseNoteOn(int* note, int* velocity, int* channel=0);
    void setNoteOn(int note, int velocity, int channel=0);

  private:
    int8 bytes[MAX_MIDI_EVENT_SIZE];
    int size;

    // Copy constructor
    MidiEvent(const MidiEvent&);
};
