#include "AudioMidiIO.h" 
#include "DSP.h"
#include "Scheduler.h" 

// TODO: * Use also RTAudio for supporting other platforms

/**
 * Audio Context
 */
AudioContext::AudioContext(){}

AudioContext::~AudioContext(){}

void AudioContext::newBlockSetupMidi(jack_nframes_t nframes){
  // Midi events arrive with a timestamp in samples, counting from the 
  // start of the audio block. Since we want that time to be in sync with
  // our TimeMan time, we can't just process all Midi events now. Instead,
  // we get the first event, wait for it's time to arrive, process it and
  // then get the next event, wait for it's time, etc.
  midiInBuffer = jack_port_get_buffer(midiInPort, nframes);
  midiEventCount = jack_midi_get_event_count(midiInBuffer);
  midiEventIndex = 0;
  if(midiEventCount>0){
    jack_midi_event_t event;
    jack_midi_event_get(&event, midiInBuffer, midiEventIndex);
    incomingMidiEvent.setRaw(event.buffer, event.size, event.time);
  }

  midiOutBuffer = jack_port_get_buffer(midiOutPort, nframes);
  jack_midi_clear_buffer(midiOutBuffer);
}

void AudioContext::tick(int frame){
  // Wait for the current Midi event time.
  int t = incomingMidiEvent.time;
  while(t==frame && midiEventIndex<midiEventCount){
    dsp->incomingMidi(&incomingMidiEvent);
    midiEventIndex++;

    // Are there still Midi events to process? get the next one.
    if(midiEventIndex<midiEventCount){
      jack_midi_event_t event;
      jack_midi_event_get(&event, midiInBuffer, midiEventIndex);
      incomingMidiEvent.setRaw(event.buffer, event.size, event.time);
    }
  }

  TimeMan->tick();
}

void AudioContext::outputMidi(MidiEvent* evt){
  int8* bytes;
  int size, time;
  evt->getRaw(&bytes, &size, &time);
  jack_midi_event_write(midiOutBuffer, time, bytes, size);
}

/**
 * AudioMidiIO
 */
AudioMidiIO::AudioMidiIO(){}

int AudioMidiIO::start(){

  if((context.jackClient = jack_client_open("Cpp Midi", JackNullOption, NULL)) == NULL){
    return(1);
  }

  // Populate context data to pass to process callback
  context.sampleRate = jack_get_sample_rate(context.jackClient);
  context.bufferSize = jack_get_buffer_size(context.jackClient);
  context.dsp = &dsp;
  context.isFirstCallback = true;

  dsp.audioParametersChanged(context.bufferSize, context.sampleRate);

  // Register ports:
  context.midiInPort = jack_port_register(context.jackClient, "Midi In", JACK_DEFAULT_MIDI_TYPE, JackPortIsInput, 0);
  context.midiOutPort = jack_port_register(context.jackClient, "Midi Out", JACK_DEFAULT_MIDI_TYPE, JackPortIsOutput, 0);

  // Register callbacks:
  jack_on_shutdown(context.jackClient, AudioMidiIO::jackShutdown, &context);
  jack_set_process_callback(context.jackClient, AudioMidiIO::jackProcess, &context);
  jack_set_sample_rate_callback(context.jackClient, AudioMidiIO::jackSampleRate, &context);
  jack_set_buffer_size_callback(context.jackClient, AudioMidiIO::jackBufferSize, &context);
  jack_set_xrun_callback(context.jackClient, AudioMidiIO::jackXrun, &context);

  // Go!
  if (jack_activate(context.jackClient)) {
    return 1;
  }

  return 0;
}

void AudioMidiIO::stop(){
  // TODO: nicely deallocate ports and jack client
}

int AudioMidiIO::jackSampleRate(jack_nframes_t newSampleRate, void *arg){
  AudioContext* context = (AudioContext*)arg;
  context->sampleRate = newSampleRate;
  context->dsp->audioParametersChanged(context->bufferSize, context->sampleRate);

  TimeMan->setSampleRate(newSampleRate);
  return 0;
}

int AudioMidiIO::jackBufferSize(jack_nframes_t newBufferSize, void *arg){
  AudioContext* context = (AudioContext*)arg;
  context->bufferSize = newBufferSize;
  context->dsp->audioParametersChanged(context->bufferSize, context->sampleRate);
  return 0;
}

void AudioMidiIO::jackShutdown(void *arg){
  // TODO: Send a message to GUI thread so that it stops the jack
  // client, deallocates, etc. Not safe to do that from here.
  // (call AudioMidiIO::stop from gui context)
  // AudioContext* context = (AudioContext*)arg;
}

int AudioMidiIO::jackXrun(void *arg){
  AudioContext* c = (AudioContext*)arg;
  unsigned int t = jack_last_frame_time(c->jackClient);
  TimeMan->setCurrentTime(t);
  return 0;
}

int AudioMidiIO::jackProcess(jack_nframes_t nframes, void *arg){
  AudioContext* context = (AudioContext*)arg;

  if(context->isFirstCallback){
    context->isFirstCallback = false;

    unsigned int t = jack_last_frame_time(context->jackClient);
    TimeMan->setCurrentTime(t);

    context->dsp->setup(context);
    context->dsp->audioContext = context;
  }

  context->newBlockSetupMidi(nframes);

  return context->dsp->process(nframes);
}

